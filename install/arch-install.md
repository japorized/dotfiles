# Setting up iwd for wireless connection

- Set up `/etc/hosts`
  ```
  127.0.0.1     localhost
  ::1           localhost
  ```
- Will need `systemd-resolved` to work in tandem for DNS
  resolution
  ```
  systemctl enable --now systemd-resolved.service
  ```
  - We'll also need a tweak...
  ```
  ln -sf ../run/systemd/resolve/stub-resolv.conf /etc/resolv.conf
  ```

# Add user to groups

This will also add the user to some commonly used groups

```
useradd -a -G users <username>
usermod -a -G docker,nix-users,informant,vboxusers,wheel <username>
```

# Display manager — SDDM

```
systemctl enable sddm.service
```
