#!/usr/bin/env bash

if ($(pamixer --get-mute)); then
  echo -n ""
else
  echo -n "  $(pamixer --get-volume-human)"
fi
