#!/usr/bin/env bash

_mkb() {
  SIZE=20 START="" END="" CHAR2="" CHAR1="" SEP="▮" mkb "$@"
}

if ($(pamixer --get-mute)); then
  echo -n "婢 $(_mkb 0) 0%"
else
  current_volume="$(pamixer --get-volume)"
  echo -n "墳 $(_mkb "$current_volume") $current_volume%"
fi
