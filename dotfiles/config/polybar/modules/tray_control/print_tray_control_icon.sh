#!/usr/bin/env bash

STATE_FILE="$XDG_CACHE_HOME/polybar_tray_control/is_active"

STATE_FILE_DIRNAME="$(dirname "$STATE_FILE")"
if [ ! -d "$STATE_FILE_DIRNAME" ]; then
  mkdir -p $STATE_FILE_DIRNAME
fi

if [ ! -f "$STATE_FILE" ]; then
  echo 'false' > "$STATE_FILE"
fi

tray_is_active="$(cat "$STATE_FILE")"

if ($tray_is_active); then
  echo "▲"
else
  echo "▼"
fi
