#!/usr/bin/env bash

STATE_FILE="$XDG_CACHE_HOME/polybar_tray_control/is_active"
LOG_FILE="$XDG_CACHE_HOME/polybar_tray_control/polybar_tray_control.log"

STATE_FILE_DIRNAME="$(dirname "$STATE_FILE")"
if [ ! -d "$STATE_FILE_DIRNAME" ]; then
  mkdir -p $STATE_FILE_DIRNAME
fi

if [ ! -f "$STATE_FILE" ]; then
  echo 'false' > "$STATE_FILE"
fi

tray_is_active="$(cat "$STATE_FILE")"

if ($tray_is_active); then
  echo "false" > "$STATE_FILE"
  tray_pid=$(xprop -name "Polybar tray window" _NET_WM_PID | grep -o '[[:digit:]]*')
  kill "$tray_pid"
else
  echo "true" > "$STATE_FILE"
  echo "--- $(date --iso-8601=s) ---" | tree -a "$LOG_FILE"
  MONITOR="$(bspc query -M -m focused --names)" polybar systray >> "$LOG_FILE" 2>&1
fi
