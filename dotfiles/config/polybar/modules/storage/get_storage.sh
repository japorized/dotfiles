#!/usr/bin/env bash

root_partition="/dev/nvme0n1p3"
home_partition="/dev/sda1"

get_partition_used_percentage() {
  df -akh "$@" | tail -n 1 | awk '{ print $5 }' | tr -d '\n'
}

get_partition_available_space() {
  df -akh "$@" | tail -n 1 | awk '{ print $4 }' | tr -d '\n'
}

case $1 in
  root)
    echo -n " $(get_partition_used_percentage "$root_partition") full"
    ;;
  home|*)
    echo -n " $(get_partition_available_space "$home_partition") left"
    ;;
esac
