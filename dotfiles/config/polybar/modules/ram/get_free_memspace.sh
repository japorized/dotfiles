#!/usr/bin/env bash

totalMem=$(awk '/MemTotal/ { printf "%.3f", $2/1024/1024 }' /proc/meminfo)
freeMem=$(awk '/MemAvailable/ { printf "%.3f", $2/1024/1024 }' /proc/meminfo)
percentUsed=$(echo "scale=1; $freeMem/$totalMem * 100" | bc)
echo -n " $percentUsed% available"
