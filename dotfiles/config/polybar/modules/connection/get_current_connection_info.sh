#!/usr/bin/env bash

WIFI_INTERFACE="wlan0"
WIRED_INTERFACE="enp3s0"

isWired=$(ip route list | grep default | grep "$WIRED_INTERFACE")
if [ -n "$isWired" ]; then
  echo -n "   Wired"
else
  ssid="$(iw dev wlan0 info | grep ssid | xargs | cut -d ' ' -f2-)"
  if [ -z "$ssid" ]; then
    ssid="---"
  fi

  echo -n "   $ssid"
fi
