#!/usr/bin/env bash

. "$HOME/.cache/wpgtk.color"

use_pango=false

while getopts "p" opt; do
  case "${opt}" in
    p) use_pango=true ;;
    \?) exit 1 ;;
  esac
done
shift $((OPTIND -1))

color="white"
symbol=""

brightness="$(light -G | cut -d '.' -f1)"

if ($use_pango); then
  echo -n "<span color='$color'>$symbol</span> $brightness%"
else
  echo -n "$symbol $brightness%"
fi
