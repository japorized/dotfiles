#!/usr/bin/env bash

. "$HOME/.cache/wpgtk.color"

use_pango=false

while getopts "p" opt; do
  case "${opt}" in
    p) use_pango=true ;;
    \?) exit 1 ;;
  esac
done
shift $((OPTIND -1))

foreground_color="$color15"
background_color="$color0"
symbol=""

# battery_percentage=$(acpi --battery | cut -d, -f2 | tr -d "%")
battery_percentage=$(upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep percentage | tr -d ' ' | cut -d ':' -f2- | tr -d "%")

status=$(acpi -a | cut -d" " -f3)
if [[ "$status" == "on-line" ]] ; then
  background_color="$color5"
  symbol=""
elif [[ "$battery_percentage" -lt 20 ]]; then
  background_color="$color1"
  symbol=""
elif [[ "$battery_percentage" -lt 35 ]]; then
  background_color="$color1"
  symbol=""
elif [ "$battery_percentage" -lt 50 ]; then
  background_color="$color3"
  symbol=""
elif [[ "$battery_percentage" -lt 70 ]]; then
  symbol=""
elif [[ "$battery_percentage" -gt 80 ]]; then
  background_color="$color2"
fi

if ($use_pango); then
  echo -n "<span color='$foreground_color'>$symbol</span> $battery_percentage%"
else
  echo -n "%{B${background_color}}  $symbol $battery_percentage%  %{B-}"
fi
