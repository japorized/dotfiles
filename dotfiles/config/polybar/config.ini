;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/polybar/polybar
;
;   The README contains a lot of information
;
;==========================================================

include-file = $XDG_CONFIG_HOME/polybar/modules/separator/module
include-file = $XDG_CONFIG_HOME/polybar/modules/empty_module/module
include-file = $XDG_CONFIG_HOME/polybar/modules/datetime/module
include-file = $XDG_CONFIG_HOME/polybar/modules/batt/module
include-file = $XDG_CONFIG_HOME/polybar/modules/mpd/module
include-file = $XDG_CONFIG_HOME/polybar/modules/countdown/module
include-file = $XDG_CONFIG_HOME/polybar/modules/brightness/module
include-file = $XDG_CONFIG_HOME/polybar/modules/volume/module
include-file = $XDG_CONFIG_HOME/polybar/modules/storage/module
include-file = $XDG_CONFIG_HOME/polybar/modules/ram/module
include-file = $XDG_CONFIG_HOME/polybar/modules/connection/module
include-file = $XDG_CONFIG_HOME/polybar/modules/tray_control/module

[global/wm]
margin-top = 5
margin-bottom = 5

[colors]
include-file = $XDG_CONFIG_HOME/polybar/colors

[common]
height = 30
radius = 10

font-0 = "Nimbus Sans:size=10:antialias=true;1"
font-1 = "FontAwesome::size=10:antialias=false;1"
font-2 = "M+ 2m:size=10;1"
font-3 = "Hack Nerd Font:size=10;1"

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 3
line-color = #f00

border-size = 0
border-color = #00000000

padding-left = 2
padding-right = 2

module-margin-left = 2
module-margin-right = 2

cursor-click = pointer
cursor-scroll = ns-resize

monitor = ${env:MONITOR:eDP-1}
override-redirect = true

[bar/countdown]
inherit = common
width = 200
height = 30
offset-x = 100%:-205
offset-y = 99%:-25
modules-center = countdown

[bar/nowplaying]
inherit = common
width = 550
height = 30
offset-y = 99%:-25
modules-left = mpd

[bar/wm]
inherit = common
width = 225
fixed-center = true
offset-x = 15
offset-y = 5

modules-center = bspwm

scroll-up = bspc desktop -f prev.local
scroll-down = bspc desktop -f next.local

wm-restack = bspwm

[bar/date_battery]
inherit = common
width = 275
offset-x = 100%:-290
offset-y = 5
fixed-center = true

modules-center = datetime batt tray_control

[bar/sys_status]
inherit = common
width = 700
offset-x = 50%:-350
offset-y = 99%:-25

modules-center = brightness volume | connection | storage/root storage/home ram

[bar/systray]
inherit = common
monitor-strict = false
width = 250
offset-x = 100%:-265
offset-y = 35
override-direct = true
tray-position = left
tray-detached = false
tray-padding = 2
tray-scale = 1.0
modules-right = empty

[bar/vol_indicator]
inherit = common
monitor-strict = false
width = 300
height = 40
modules-center = volume_subscriber
offset-x = 100%:-315
offset-y = 99%:-35
enable-ipc = true

; BUILT-IN MODULES

[module/xwindow]
type = internal/xwindow
label = %title:0:30:...%

[module/bspwm]
type = internal/bspwm

ws-icon-0 = "I;"
ws-icon-1 = "II;"
ws-icon-2 = "III;"
ws-icon-3 = "IV;"
ws-icon-4 = "V;"
ws-icon-5 = "VI;"
ws-icon-6 = "VII;"
ws-icon-7 = "VIII;●"
ws-icon-8 = "IX;●"
ws-icon-9 = "X;●"

format = <label-state> <label-mode>

label-focused = %icon%
label-focused-background= ${colors.color5}
label-focused-foreground= ${colors.foreground}
label-focused-padding = 2

label-urgent = %icon%
label-urgent-background = ${colors.color1}
label-urgent-padding = 2

label-occupied = %icon%
label-occupied-foreground = ${colors.color6}
label-occupied-padding = 2

label-empty = %icon%
label-empty-foreground = ${colors.foreground}
label-empty-padding = 2

; Separator in between workspaces
; label-separator = |

[module/xbacklight]
type = internal/xbacklight

format = <label> <bar>
label = BL

bar-width = 10
bar-indicator = |
bar-indicator-foreground = ${colors.color3}
bar-indicator-font = 2
bar-fill = ─
bar-fill-font = 2
bar-fill-foreground = #9f78e1
bar-empty = ─
bar-empty-font = 2
bar-empty-foreground = ${colors.color7}

[module/backlight-acpi]
inherit = module/xbacklight
type = internal/backlight
card = intel_backlight

[module/cpu]
type = internal/cpu
interval = 2
format-prefix = " "
format-prefix-foreground = ${colors.color7}
format-underline = #f90000
label = %percentage:2%%

[module/memory]
type = internal/memory
interval = 2
format-prefix = " "
format-prefix-foreground = ${colors.color7}
format-underline = #4bffdc
label = %percentage_used%%

[module/wlan]
type = internal/network
interface = wlp5s0
interval = 3.0

format-connected = <ramp-signal> <label-connected>
format-connected-underline = #9f78e1
label-connected = %essid%

format-disconnected =
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${self.format-connected-underline}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${colors.color7}

ramp-signal-0 = 
ramp-signal-1 = 
ramp-signal-2 = 
ramp-signal-3 = 
ramp-signal-4 = 
ramp-signal-foreground = ${colors.color7}

[module/eth]
type = internal/network
interface = tailscale0
interval = 3.0

format-connected-underline = #55aa55
format-connected-prefix = ""
format-connected-prefix-foreground = ${colors.color7}
label-connected = %local_ip%

format-disconnected =
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${self.format-connected-underline}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${colors.color7}

[module/date]
type = internal/date
interval = 5

date = %Y-%m-%d
date-alt = " %Y-%m-%d"

time = %H:%M
time-alt = %H:%M:%S

format-prefix = " "
format-prefix-foreground = ${colors.color7}
format-underline = #0a6cf5

label = %date% %time%

[module/pulseaudio]
type = internal/pulseaudio

format-volume = <label-volume> <bar-volume>
label-volume = VOL %percentage%%
label-volume-foreground = ${root.foreground}

label-muted = 🔇 muted
label-muted-foreground = #666

bar-volume-width = 10
bar-volume-foreground-0 = #55aa55
bar-volume-foreground-1 = #55aa55
bar-volume-foreground-2 = #55aa55
bar-volume-foreground-3 = #55aa55
bar-volume-foreground-4 = #55aa55
bar-volume-foreground-5 = #f5a70a
bar-volume-foreground-6 = #ff5555
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-indicator-font = 2
bar-volume-fill = ─
bar-volume-fill-font = 2
bar-volume-empty = ─
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.color7}

[module/alsa]
type = internal/alsa

format-volume = <label-volume> <bar-volume>
label-volume = VOL
label-volume-foreground = ${root.foreground}

format-muted-prefix = " "
format-muted-foreground = ${colors.color7}
label-muted = sound muted

bar-volume-width = 10
bar-volume-foreground-0 = #55aa55
bar-volume-foreground-1 = #55aa55
bar-volume-foreground-2 = #55aa55
bar-volume-foreground-3 = #55aa55
bar-volume-foreground-4 = #55aa55
bar-volume-foreground-5 = #f5a70a
bar-volume-foreground-6 = #ff5555
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-indicator-font = 2
bar-volume-fill = ─
bar-volume-fill-font = 2
bar-volume-empty = ─
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.color7}

[module/battery]
type = internal/battery
battery = BAT0
adapter = AC
full-at = 98

format-charging = <animation-charging> <label-charging>
format-charging-underline = #ffb52a

format-discharging = <animation-discharging> <label-discharging>
format-discharging-underline = ${self.format-charging-underline}

format-full-prefix = " "
format-full-prefix-foreground = ${colors.color7}
format-full-underline = ${self.format-charging-underline}

ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-foreground = ${colors.color7}

animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-foreground = ${colors.color7}
animation-charging-framerate = 750

animation-discharging-0 = 
animation-discharging-1 = 
animation-discharging-2 = 
animation-discharging-foreground = ${colors.color7}
animation-discharging-framerate = 750

[module/temperature]
type = internal/temperature
thermal-zone = 0
warn-temperature = 60

format = <label>
format-underline = #f50a4d
format-warn = <label-warn>
format-warn-underline = ${self.format-underline}

label = %temperature-c%
label-warn = %temperature-c%
label-warn-foreground = ${colors.color3}

ramp-0 = 
ramp-1 = 
ramp-2 = 
ramp-foreground = ${colors.color7}

[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over
;pseudo-transparency = false

; vim:ft=dosini
