static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#efefef", "#1c1c1c" },
	[SchemeSel] = { "#efefef", "#af99ed" },
	[SchemeOut] = { "#efefef", "#5a5787" },
};
