#!/bin/sh
[ "${TERM:-none}" = "linux" ] && \
    printf '%b' '\e]P01c1c1c
                 \e]P1af99ed
                 \e]P28e94f2
                 \e]P39fa0ff
                 \e]P4bbadff
                 \e]P5dab6fc
                 \e]P67d7abc
                 \e]P7c1c1c1
                 \e]P86d6477
                 \e]P97c6c8f
                 \e]PA5c5f9c
                 \e]PB6869b2
                 \e]PC726d9b
                 \e]PD7c6c8f
                 \e]PE5a5787
                 \e]PFefefef
                 \ec'
