static const char* selbgcolor   = "#1c1c1c";
static const char* selfgcolor   = "#efefef";
static const char* normbgcolor  = "#8e94f2";
static const char* normfgcolor  = "#efefef";
static const char* urgbgcolor   = "#af99ed";
static const char* urgfgcolor   = "#efefef";
